# frozen_string_literal: true

require_relative '../nice_symbol'

RSpec.describe NiceSymbol do
  describe '#check' do
    context 'Test inheritance' do
      it 'NiceSymbol is SymbolC' do
        expect(described_class).to be < SymbolC
      end

      it 'SymbolC is ancestor of NiceSymbol' do
        expect(SymbolC).to be > NiceSymbol
      end
    end
  end
end
