# frozen_string_literal: true

require_relative 'symbol'

# symbol with support of fontsize and font name
class NiceSymbol < SymbolC
  attr_accessor :font, :fontsize

  def initialize(val_ = 'a', font_ = 'Iosevka Slab', fontsize_ = 14)
    super(val_)
    @font = font_
    @fontsize = fontsize_
  end
end
