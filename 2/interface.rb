# frozen_string_literal: true

require_relative 'nice_symbol'
require_relative 'symbol'

A = SymbolC.new 'b'
A.print
puts A.code
A.val = 'f'
puts A.code
