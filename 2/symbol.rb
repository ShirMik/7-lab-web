# frozen_string_literal: true

# class of simple symbol
class SymbolC
  attr_accessor :val

  def initialize(val_ = 'a')
    @val = val_
  end

  def print
    puts @val
  end

  def code
    @val.ord
  end
end
