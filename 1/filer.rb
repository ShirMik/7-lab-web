# frozen_string_literal: true

# class for processing files
class Filer
  def process_text(text)
    return unless text.is_a? String

    text.split("\n").collect(&:reverse).join("\n")
  end

  def process_file(filename_r, filename_w)
    return unless (filename_r.is_a? String) && File.file?(filename_r) && !File.file?(filename_w)

    file = File.new(filename_r)
    text = file.read
    file.close

    text = process_text(text)

    file = File.new(filename_w, 'w')
    file.write(text)
    file.close
  end
end

if __FILE__ == $PROGRAM_NAME
  filer = Filer.new
  puts 'proceed!'
  filer.process_file('F', 'G')
end
