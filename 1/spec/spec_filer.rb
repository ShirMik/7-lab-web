# frozen_string_literal: true

require_relative '../filer'
require 'faker'

RSpec.describe Filer do
  def safety_func(filename1, text)
    return if File.file?(filename1)

    tmp_file = File.new(filename1, 'w')
    tmp_file.write(text)

    tmp_file.close
  end

  describe 'task_1' do
    filename1 = 'F'
    filename2 = 'G'
    let(:text) { Faker::Lorem.sentences(number: 16).join("\n") }

    it 'test func' do
      safety_func(filename1, text)
      described_class.new.process_file(filename1, filename2)

      expect(
        File.file?(filename1) && File.file?(filename2) && \
        (File.readlines(filename1).count == File.readlines(filename2).count) && \
        (0..(File.readlines(filename1).count - 1)).all? do |x|
          File.readlines(filename1, chomp: true)[x] == \
            File.readlines(filename2, chomp: true)[x].reverse
        end
      ).to be_truthy
    end

    it 'no file' do
      File.delete(filename1) if File.file?(filename1)
      File.delete(filename2) if File.file?(filename2)
      described_class.new.process_file(filename1, filename2)
      expect(!File.file?(filename2)).to be_truthy
    end
  end
end
